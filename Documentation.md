# AudioMob

## Integration Steps

1) **"Install"** or **"Upload"** FG AudioMob plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import AudioMob SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG AudioMob module from the Integration Manager window, you will find the last compatible version of AudioMob SDK in the _Assets > FunGames_Externals > AudioAds_ folder. Double click on the .unitypackage file to install it.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

Ask your Publisher to create your app on the AudioMob account and to provide you the latest Unity Package and credentials for your app.

You will then need to add your Api Key in the **FGAudioMobSettings** (_Assets > Resources > FGAudioMobSettings_).